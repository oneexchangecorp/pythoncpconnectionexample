import websocket
import requests
import time
import urllib
import json
import threading

DOMAIN = "REPLACE WITH CLIENT PORTAL-API DOMAIN"
USERNAME = "REPLACE WITH USERNAME"
PASSWORD = "REPLACE WITH PASSWORD"

auth_token = ""

def login():
    global auth_token

    payload = {
        "username" : USERNAME,
        "password" : PASSWORD
    }

    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    data = urllib.parse.urlencode(payload)

    try:
        response = requests.post(f"https://{DOMAIN}/api/login", data=data, headers=headers)
        response_string = response.content

        token_object = json.loads(response_string)
        auth_token = token_object["token"]
        print(f"Found Key: {auth_token}")
    except Exception as e:
        print(f"An exception occurred: {e}")


def on_message(ws, message):
    print("*************** Websocket OnMessage ************************")
    print(message)

def on_error(ws, error):
    print(f"ERROR: {error}")

def on_close(ws):
    print("### CLOSED ###")
    reconnect()

def on_open(ws):
    print("*************** Websocket OnOpen ************************")

def connect():
    try:
        ws = websocket.WebSocketApp(f"wss://{DOMAIN}",
                                on_open = on_open,
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close,
                                cookie = f"token={auth_token}")
                                
        wst = threading.Thread(target=ws.run_forever)
        wst.start()
    except Exception as e:
        print(e)
   
  
def reconnect():
    time.sleep(5)
    login()
    connect()

# API CALLS
def get_confirms_by_date_range():
    header = {"Authorization": f"Bearer {auth_token}"}
    response = requests.get(f"https://" + DOMAIN + "/api/v1/confirms?from_date=02/07/2020&to_date=02/07/2020", headers=header)
    print("*************** getConfirmsByDateRange ************************")

    response_string = response.content
    print(response_string)

    print("*************** End getConfirmsByDateRange ************************")

def get_settles_by_date():
    header = {"Authorization": f"Bearer {auth_token}"}
    response = requests.get(f"https://" + DOMAIN + "/api/v1/settles?settle_date=06/17/2020", headers=header)
    print("*************** getSettlesbyDate ************************")

    response_string = response.content
    print(response_string)

    print("*************** End getSettlesbyDate ************************")

def get_indeces_by_date():
    header = {"Authorization": f"Bearer {auth_token}"}
    response = requests.get(f"https://" + DOMAIN + "/api/v1/cci_index?date=05/06/2020", headers=header)
    print("*************** getIndicesbyDate ************************")

    response_string = response.content
    print(response_string)

    print("*************** End getIndicesbyDate ************************")

if __name__ == "__main__":

    login()

    connect()

    get_confirms_by_date_range()
    get_settles_by_date()
    get_indeces_by_date()

    



