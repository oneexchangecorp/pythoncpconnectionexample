# Python Client Portal Connection Example #

A Python implementation of how to connect to the One Exchange Client Portal

### Installation ###

Replace domain, username and password with values provided by One Exchange in the main.py file.
Install websocket by running pip install websocket.

### Usage ###

run main.py